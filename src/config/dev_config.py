#!/usr/bin/env python

from .config import Config


class DevConfig(Config):
    """
    Dev config
    """

    DEBUG = True
    MONGO = {
        'replset': 'rs0',
        'nodes': ["127.0.0.1:27017",
                  "127.0.0.1:27018",
                  "127.0.0.1:27019"],
        'dbname': 'mercury',
        'username': 'mercury',
        'password': 'mercury123'
    }

#!/usr/bin/env python

import os


class Config():
    """
    Basic config
    """
    TIMEZONE = 'Asia/Shanghai'
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
